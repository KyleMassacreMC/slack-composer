<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Http\Request;

class ExampleController extends Controller
{
    public $responseType = "in_channel";

    public $attachment = [];

    public $response = [];

    public $text = "";

    public $actionWord = "";

    public $baseURI = "https://packagist.org/packages/";

    public $timeout = 2.0;

    public $command = NULL;

    public $version = NULL;

    public $client;

    public $helpText = [];
    
    public $devFlag;

    public $searchFlag;

    public $page;

    public $request;

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct(Request $request)
    {
        $this->helpText = [
            'require' => [
                "text" => "The `require` command brings up information based on the package you provide. \n *Usage:* \n ```/composer require <vendor>/<package>```". PHP_EOL,
                "tags" => "Available flags: `--help`,`--version=<version>`,`--quiet`"
            ],
            'licenses' => [
                "text" => "The `licenses` command brings up all the licenses that are associated with the given package .\n *Usage:* \n ```/composer licenses <vendor>/<package>```". PHP_EOL,
                "tags" => "Available flags: `--help`,`--version=<version>`,`--quiet`"
            ],
            'browse' => [
                "text" => "The `browse` command will bring up links (if provided) to the given package. \n *Usage:* \n ```/composer licenses <vendor>/<package>```". PHP_EOL,
                "tags" => "Available flags: `--help`,`--version=<version>`,`--quiet`"
            ],
            'depends' => [
                "text" => "The `depends` command will bring up the dependencies to the given package. \n *Usage:* \n ```/composer depends <vendor>/<package>```". PHP_EOL,
                "tags" => "Available flags: `--help`,`--version=<version>`,`--dev-only`,`--no-dev`,`--quiet`"
            ],
            'suggest' => [
                "text" => "The `suggest` command will show package suggestions if provided. \n *Usage:* \n ```/composer suggest <vendor>/<package>```". PHP_EOL,
                "tags" => "Available flags: `--help`,`--version=<version>`,`--quiet`,`--page=<int>`"
            ],
            'search' => [
                "text" => "The `search` command will search for packages based on a vendor.  \n *Usage:* \n ```/composer search <vendor>```". PHP_EOL,
                "tags" => "Available flags: `--help`,`--tag`,`--name`,`--vendor`,`--quiet`"
            ],
            'help' => [
                "text" => "The `help` command will display the help menu.  \n *Usage:* \n ```/composer help```". PHP_EOL,
                "tags" => "Available flags: None"
            ],
        ];
        $this->command = $request->has('command') ? $request->input('command') : NULL;
        if ($request->has('text'))
        {
            $this->text = explode(" ", $request->input('text'));
            $this->actionWord = $this->text[0];
            $this->client = new Client([
                'base_uri' => $this->baseURI,
                'timeout' => $this->timeout,
            ]);
            if(str_contains($request->input('text'),'--no-dev'))
            {
                $this->devFlag = 'noDev';
            }
            elseif(str_contains($request->input('text'),'--dev-only'))
            {
                $this->devFlag = 'devOnly';
            }
            else
            {
                $this->devFlag = 'none';
            }
            if (str_contains($request->input('text'), ['--version=']))
            {
                $ver = preg_match('^((([\w]+)\.([\w]+)\.([\w]+)(?:-([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?)(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?)$^',
                    $request->input('text'),
                    $version
                );
                if ($ver == true)
                {
                    $this->version = $version[0];
                }
            }
            if(str_contains($request->input('text'),['--quiet']))
            {
                $this->responseType = "ephemeral";
            }

            if(str_contains($request->input('text'),['--tag']))
            {
                $this->searchFlag = 'tag';
            }
            elseif(str_contains($request->input('text'),['--name']))
            {
                $this->searchFlag = 'name';
            }
            else
            {
                $this->searchFlag = 'vendor';
            }
            if (str_contains($request->input('text'), ['--page=']))
            {
                $page = preg_match('^([0-9]+)^',
                    $request->input('text'),
                    $pages
                );
                if ($page == true && $pages[0] >= 1)
                {
                    $this->page = abs((int)$pages[0]);
                }
                else
                {
                    $this->page = 1;
                }
            }
        }

    }

    public function test(Request $request)
    {

        if (method_exists($this, 'composer' . ucfirst($this->actionWord)))
        {
            if(!isset($this->text[1]) || !array_key_exists(1,$this->text))
            {
                $return = $this->composerHelp();
            }
            elseif(str_contains($request->input('text'),['--help']) && isset($this->actionWord))
            {
                $this->responseType = "ephemeral";
                $return = [
                    'response_type' => $this->responseType,
                    'text' => array_get($this->helpText[$this->actionWord],'text','None').array_get($this->helpText[$this->actionWord],'tags','None'),
                    'mrkdwn' => true,
                    'attachments' => [$this->attachment]
                ];
            }
            else
            {
                $method = 'composer' . ucfirst($this->actionWord);
                $return = $this->$method();
            }
        }
        else
        {
            $return = $this->composerHelp();
        }
        return response()->json($return);


    }

    public function composerRequire()
    {
        $package = $this->text[1];
        try
        {
            $response = $this->client->request('GET', $package . '.json');
        } catch (BadResponseException $e)
        {
            $this->responseType = 'ephemeral';
            $this->response = [
                'response_type' => $this->responseType,
                'text' => $package . ' was not found'
            ];
            return $this->response;
        }

        $data = json_decode($response->getBody(), true);
        $packageData = array_first($data['package']['versions']);
        if ($this->version != NULL)
        {
            $packageData = array_key_exists($this->version, $data['package']['versions']) ? $data['package']['versions'][$this->version] : $packageData;
        }
        $this->attachment = [
            'fallback' => 'oops. this bot sucks.',
            'title' => $data['package']['name'],
            'color' => 'good',
            'mrkdwn_in' => ['text'],
            'text' => $data['package']['description'] . "\n```$ composer {$this->actionWord} " . $data['package']['name'] . ":{$packageData['version']}```",
            'fields' => [
                ['title' => 'Version', 'value' => $packageData['version'], 'short' => true],
                ['title' => 'Today', 'value' => number_format($data['package']['downloads']['daily']), 'short' => true],
                ['title' => 'Monthly', 'value' => number_format($data['package']['downloads']['monthly']), 'short' => true],
                ['title' => 'Total', 'value' => number_format($data['package']['downloads']['total']), 'short' => true]
            ],
            'thumb_url' => array_get($packageData, 'extra.slack-image.thumb', '')
        ];

        $this->response = [
            'response_type' => $this->responseType,
            'text' => $data['package']['name'],
            'mrkdwn' => true,
            'attachments' => [$this->attachment]
        ];

        return $this->response;

    }

    public function composerBrowse()
    {
        $package = $this->text[1];
        try
        {
            $response = $this->client->request('GET', $package . '.json');
        } catch (BadResponseException $e)
        {
            $this->responseType = 'ephemeral';
            $this->response = [
                'response_type' => $this->responseType,
                'text' => $package . ' was not found'
            ];
            return $this->response;
        }

        $data = json_decode($response->getBody(), true);
        $packageData = array_first($data['package']['versions']);
        if ($this->version != NULL)
        {
            $packageData = array_key_exists($this->version, $data['package']['versions']) ? $data['package']['versions'][$this->version] : $packageData;
        }
        $homePage = array_get($packageData,'homepage',false);
        $repo = array_get($packageData,'source.url',false);
//        $homePage = array_key_exists('homepage',$packageData) ? $packageData['homepage'] : false;
//        $repo = array_key_exists('url',$packageData['source']) ? $packageData['source']['url']: false;
        $repoText = $repo != false ? "<$repo|Repo>" : "None";
        $homePageText = $homePage != false ? "<$homePage|Homepage>" : "None";

        $this->attachment = [
            'fallback' => 'oops. this bot sucks.',
            'title' => $data['package']['name'],
            'color' => 'good',
            'mrkdwn_in' => ['text'],
            'text' => $data['package']['description'] . "\n```$ composer {$this->actionWord} " . $data['package']['name']."```",
            'fields' => [
                ['title' => 'Homepage', 'value' => $homePageText, 'short' => true],
                ['title' => 'Repository', 'value' => $repoText, 'short' => true],
            ],
            'thumb_url' => array_get($packageData, 'extra.slack-image.thumb', '')
        ];
        $this->response = [
            'response_type' => $this->responseType,
            'text' => $data['package']['name'],
            'mrkdwn' => true,
            'attachments' => [$this->attachment]
        ];

        return $this->response;
    }

    public function composerLicenses()
    {
        $package = $this->text[1];
        try
        {
            $response = $this->client->request('GET', $package . '.json');
        } catch (BadResponseException $e)
        {
            $this->responseType = 'ephemeral';
            $this->response = [
                'response_type' => $this->responseType,
                'text' => $package . ' was not found'
            ];
            return $this->response;
        }

        $data = json_decode($response->getBody(), true);
        $packageData = array_first($data['package']['versions']);
        if ($this->version != NULL)
        {
            $packageData = array_key_exists($this->version, $data['package']['versions']) ? $data['package']['versions'][$this->version] : $packageData;
        }

        $this->attachment = [
            'fallback' => 'oops. this bot sucks.',
            'title' => $data['package']['name'],
            'color' => 'good',
            'mrkdwn_in' => ['text'],
            'text' => $data['package']['description'] . "\n```$ composer {$this->actionWord} " . $data['package']['name']."```",
            'fields' => [
                ['title' => 'License', 'value' => implode(',',$packageData['license']), 'short' => true],
            ],
            'thumb_url' => array_get($packageData, 'extra.slack-image.thumb', '')
        ];
        $this->response = [
            'response_type' => $this->responseType,
            'text' => $data['package']['name'],
            'mrkdwn' => true,
            'attachments' => [$this->attachment]
        ];

        return $this->response;

    }

    public function composerSuggest()
    {
        $package = $this->text[1];
        try
        {
            $response = $this->client->request('GET', $package . '.json');
        } catch (BadResponseException $e)
        {
            $this->responseType = 'ephemeral';
            $this->response = [
                'response_type' => $this->responseType,
                'text' => $package . ' was not found'
            ];
            return $this->response;
        }
    
        $data = json_decode($response->getBody(), true);
        $packageData = array_first($data['package']['versions']);
        if ($this->version != NULL)
        {
            $packageData = array_key_exists($this->version, $data['package']['versions']) ? $data['package']['versions'][$this->version] : $packageData;
        }
        $suggestion = [];
        if(array_get($packageData,'suggest'))
        {
            foreach ($packageData['suggest'] as $k => $v)
            {
                $suggestion['title'] = $k;
                $suggestion['value'] = $v;
                $suggestion['short'] = true;
            }

        }
        else
        {
            $suggestion['title'] = "None";
            $suggestion['value'] = "N/A";
            $suggestion['short'] = true;
        }
    
        $this->attachment = [
            'fallback' => 'oops. this bot sucks.',
            'title' => $data['package']['name'],
            'color' => 'good',
            'mrkdwn_in' => ['text'],
            'text' => $data['package']['description'] . "\n```$ composer {$this->actionWord} " . $data['package']['name']."```",
            'fields' => [
                $suggestion,
            ],
            'thumb_url' => array_get($packageData, 'extra.slack-image.thumb', '')
        ];
        $this->response = [
            'response_type' => $this->responseType,
            'text' => $data['package']['name'],
            'mrkdwn' => true,
            'attachments' => [$this->attachment]
        ];

        return $this->response;
    }

    public function composerSearch()
    {
        $package = $this->text[1];
        switch($this->searchFlag)
        {
            case 'tag':
                $uri = 'https://packagist.org/search.json?tags=';
                $key = 'results';
                break;
            case 'name':
                $uri = 'https://packagist.org/search.json?q=';
                $key = 'results';
                break;
            case 'vendor':
                $uri = 'https://packagist.org/packages/list.json?vendor=';
                $key = 'packageNames';
                break;
            default:
                $uri = 'https://packagist.org/packages/list.json?vendor=';
                $key = 'packageNames';
                break;
        }
        try
        {
            $page = isset($this->page) ? '&page='.abs((int)$this->page) : '';
            $response = $this->client->request('GET', $uri.$package.$page);
        } catch (BadResponseException $e)
        {
            $this->responseType = 'ephemeral';
            $this->response = [
                'response_type' => $this->responseType,
                'text' => "Results for tag: ".$package . ' was not found'
            ];
            return $this->response;
        }

        $data = json_decode($response->getBody(), true);
        $results = [];
        $packages = $data[$key];
        if($this->searchFlag != 'vendor')
        {
                foreach ($packages as $k => $v)
                {
                    $values = [
                        "*Description:* ".$v['description'],
                        "*Downloads:* ".$v['downloads'],
                        "*Repo:* <{$v['repository']}|Repository>"
                    ];

                    $results[] = array_add(['title' => $v['name'], 'value' => implode("\n",$values)], 'short', true);
                }
            $pages = ceil($data['total']/15);
            if($this->page > $pages)
            {
                $pageNumber = $pages;
            }
            elseif($this->page < 1)
            {
                $pageNumber = 1;
            }
            else
            {
                $pageNumber = $this->page;
            }
            $this->attachment = [
                'fallback' => 'oops. this bot sucks.',
//            'title' => $data['package']['name'],
                'color' => 'good',
                'mrkdwn_in' => ['text','fields'],
                'text' => "Results:".$data['total'].". Showing page ".$pageNumber." of ".$pages,
                'fields' =>
                    $results,
            ];
        }
        else
        {
            foreach ($packages as $v)
            {
                $values = [
                    "*Package Name:* ".$v,
                    "*URL:* <https://packagist.org/packages/{$v}|Packagist>"

                ];

                $results[] = array_add(['value' => implode("\n",$values)], 'short', true);
            }
            $this->attachment = [
                'fallback' => 'oops. this bot sucks.',
                'title' => $package. ' Packages:',
                'color' => 'good',
                'mrkdwn_in' => ['text','fields'],
                'text' => "Results:".count($packages).".",
                'fields' =>
                    $results,
            ];
        }
        $this->response = [
            'response_type' => $this->responseType,
//            'text' => $data['package']['name'],
            'mrkdwn' => true,
            'attachments' => [$this->attachment]
        ];


        return $this->response;
    }

    public function composerHelp()
    {
        $this->responseType = 'ephemeral';
        $data['text'] = "*Usage:* \n";
        $data['text'] .= "```$ " . $this->command . " [action] [options]``` \n";
        $data['text'] .= "\n *Options:* \n";
        $data['text'] .= "```";
        $data['text'] .= "--help              Displays this help message" . PHP_EOL;
        $data['text'] .= "--quiet             Only shows information to you" . PHP_EOL;
        $data['text'] .= "--no-dev            Doesn't show the dev dependencies" . PHP_EOL;
        $data['text'] .= "--dev-only          Only shows the dev dependencies" . PHP_EOL;
        $data['text'] .= "--tag               Searches for packages with the tag provided" . PHP_EOL;
        $data['text'] .= "--name              Searches for packages with the name provided" . PHP_EOL;
        $data['text'] .= "--vendor            Searches for all packages by the vendor provided" . PHP_EOL;
        $data['text'] .= "--version=<ver>     Finds a version for package. If none is found, will return latest" . PHP_EOL;
        $data['text'] .= "```";
        $data['text'] .= "\n *Available actions:* \n";
        $data['text'] .= "```";
        $data['text'] .= "help      Displays this help message" . PHP_EOL;
        $data['text'] .= "require   Displays information for given package: <vendor>/<name>" . PHP_EOL;
        $data['text'] .= "browse    Displays a link to the package homepage" . PHP_EOL;
        $data['text'] .= "licenses  Displays the license of the package" . PHP_EOL;
        $data['text'] .= "search    Search for a package" . PHP_EOL;
        $data['text'] .= "suggest   Shows package suggestions" . PHP_EOL;
        $data['text'] .= "depends   The `depends` command will bring up the dependencies to the given package" . PHP_EOL;
        $data['text'] .= "```";

        $this->response = [
            'response_type' => $this->responseType,
            'text' => $data['text'],
            'mrkdwn' => true,
        ];
        return $this->response;


    }
    
    public function composerDepends()
    {
        $package = $this->text[1];
        try
        {
            $response = $this->client->request('GET', $package . '.json');
        } catch (BadResponseException $e)
        {
            $this->responseType = 'ephemeral';
            $this->response = [
                'response_type' => $this->responseType,
                'text' => $package . ' was not found'
            ];
            return $this->response;
        }
    
        $data = json_decode($response->getBody(), true);
        $packageData = array_first($data['package']['versions']);
        if ($this->version != NULL)
        {
            $packageData = array_key_exists($this->version, $data['package']['versions']) ? $data['package']['versions'][$this->version] : $packageData;
        }
        $fields = [];

        if($this->devFlag == 'none' || $this->devFlag == 'noDev')
        {
            if (array_get($packageData, 'require'))
            {
                foreach ($packageData['require'] as $k => $v)
                {
                    $fields[] = array_add(['title' => $k, 'value' => $v], 'short', true);
                }
        
            }
            else
            {
                $fields['title'] = "None";
                $fields['value'] = "N/A";
                $fields['short'] = true;
            }
        }

        if($this->devFlag != 'noDev')
        {
            if (array_get($packageData, 'require-dev'))
            {

                foreach ($packageData['require-dev'] as $k => $v)
                {
                    $fields[] = array_add(['title' => $k, 'value' => '_'.$v.'_'], 'short', true);
                }

            }
            else
            {
                $fields['title'] = "None";
                $fields['value'] = "N/A";
                $fields['short'] = true;
            }
        }
    
        $this->attachment = [
            'fallback' => 'oops. this bot sucks.',
            'title' => $data['package']['name'],
            'color' => 'good',
            'mrkdwn_in' => ['text','fields'],
            'text' => $data['package']['description'] . "\n```$ composer {$this->actionWord} " . $data['package']['name']."```",
            'fields' =>
                $fields,
            'thumb_url' => array_get($packageData, 'extra.slack-image.thumb', '')
        ];
        $this->response = [
            'response_type' => $this->responseType,
            'text' => $data['package']['name'],
            'mrkdwn' => true,
            'attachments' => [$this->attachment]
        ];
    
        return $this->response;
    }
}
